package com.rs.rocky.rsrock.api.song;

import com.rs.rocky.rsrock.api.song.model.SongDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value={"/song/"})
public class SongController {

  @Autowired
  SongService songService;
  @ApiOperation(value = "Request a list od songs based on the genre provided in the request. ", response = SongDTO.class)
  @GetMapping("/")
  public ResponseEntity<List<SongDTO>> getSongs(@RequestParam(value="genre", defaultValue = "Metal") String genre){
    return ResponseEntity.ok(songService.getSongsWithGenre(genre));
  }

  @ApiOperation(value = "Request song by Id. ", response = SongDTO.class)
  @GetMapping("{id}")
  public ResponseEntity<SongDTO> getSongs(@PathVariable("id") long id){
    return ResponseEntity.ok(songService.getSongWithId(id));
  }

  @ApiOperation(value = "Delete a song by Id. ", response = SongDTO.class)
  @DeleteMapping("{id}")
  public ResponseEntity<SongDTO> deleteSongWithId(@PathVariable("id") long id){
    return ResponseEntity.ok(songService.deleteSongWithId(id));
  }

  @ApiOperation(value = "Add a song.", response = SongDTO.class)
  @PostMapping()
  public ResponseEntity<SongDTO> addNewSong(@RequestBody SongDTO newSongDTO){
    return ResponseEntity.ok(songService.addSong(newSongDTO));
  }
}