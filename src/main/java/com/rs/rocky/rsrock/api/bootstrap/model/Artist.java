package com.rs.rocky.rsrock.api.bootstrap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Artist {
  @JsonProperty("Id")
  private long id;
  @JsonProperty("Name")
  private String name;
}
