package com.rs.rocky.rsrock.api.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value={"/bootstrap/"})
public class BootstrapController {

  @Autowired
  BootstrapService bootstrapService;

  @GetMapping("/ingestdata")
  public ResponseEntity ingestInitialData(){
    return ResponseEntity.ok().body(Optional.of(bootstrapService.bootStrapData()));
  }
}
