package com.rs.rocky.rsrock.api.bootstrap;

import com.rs.rocky.rsrock.api.bootstrap.model.*;
import com.rs.rocky.rsrock.repositories.ArtistRepository;
import com.rs.rocky.rsrock.repositories.SongRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.util.List;
import java.util.Optional;

@Service
public class BootstrapService {

  @Autowired
  ArtistRepository artistRepository;

  @Autowired
  SongRepository songRepository;

  @Autowired
  RestTemplate restTemplate;

  @Autowired
  ModelMapper modelMapper;



  @Value("${application.bootstrap.data.artists}"  )
  String artistsLocation;

  @Value("${application.bootstrap.data.songs}"  )
  String songsLocation;

  public String bootStrapData(){
    loadSongs().ifPresent(songsList -> songsList.stream()
            .filter(song -> isMetalAndBefore2016(song))
            .forEach(song ->
                    songRepository.save(modelMapper.map(song, SongEntity.class))));

    loadArtists().ifPresent(artistList -> artistList.stream()
            .forEach(artist ->
                    artistRepository.save(modelMapper.map(artist, ArtistEntity.class))));

    return "";
  }

  public Optional<List<Artist>> loadArtists(){
    ResponseEntity<List<Artist>> artistsResponseEntity = restTemplate.exchange(artistsLocation, HttpMethod.GET, null, new ParameterizedTypeReference<List<Artist>>() { });
    return artistsResponseEntity.getStatusCode().is2xxSuccessful() ? Optional.of(artistsResponseEntity.getBody()) : Optional.empty();
  }

  private Optional<List<Song>> loadSongs(){
    ResponseEntity<List<Song>> songsResponseEntity = restTemplate.exchange(songsLocation, HttpMethod.GET, null, new ParameterizedTypeReference<List<Song>>() { });
    return songsResponseEntity.getStatusCode().is2xxSuccessful() ? Optional.of(songsResponseEntity.getBody()) : Optional.empty();
  }


  protected boolean isMetalAndBefore2016(Song song){
    return (song.getGenre().toLowerCase().indexOf("metal") > -1) &&
            song.getYear() < 2016;
  }
}
