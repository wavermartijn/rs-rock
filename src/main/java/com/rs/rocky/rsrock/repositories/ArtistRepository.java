package com.rs.rocky.rsrock.repositories;

import com.rs.rocky.rsrock.api.bootstrap.model.ArtistEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<ArtistEntity,Long> {
}
