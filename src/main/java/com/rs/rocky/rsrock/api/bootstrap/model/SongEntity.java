package com.rs.rocky.rsrock.api.bootstrap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "songs")
public class SongEntity {
  @Id
  private long id;

  @Column
  private String name;

  @Column
  private Short year;

  @Column
  private String artist;

  @Column
  private String shortName;

  @Column
  private Short bpm;

  @Column
  private Long duration;

  @Column
  private String genre;

  @Column
  private String spotifyId;

  @Column
  private String album;
}
