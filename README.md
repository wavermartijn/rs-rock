# RS-Rock #

RS-rock can be started using the gradle command `gradle bootRun -Dspring.profile.active={ENVIRONMENTNAME}`. 
The latest released version of the application can also be started using the `waver/gc-rock` Docker image. This

### What 

* This service ingests the data provided by Team RockStars IT and serves metal rock data.
* All endpoints can be explored using swagger on `/swagger-ui.html`


### How do I get set up? ###

* compile the application
* release the application using `gradle jib`
* ingest the data in memory by calling `http://localhost:8080/ingestdata`

