package com.rs.rocky.rsrock.repositories;

import com.rs.rocky.rsrock.api.bootstrap.model.SongEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SongRepository extends JpaRepository<SongEntity,Long> {
  List<SongEntity> findByGenre(String genre);
}
