USE public;

DROP TABLE IF EXISTS `artists`;
CREATE TABLE `artists` ( `id` real PRIMARY KEY, `name` VARCHAR(50) );

DROP TABLE IF EXISTS `songs`;
CREATE TABLE `songs` (`id` REAL PRIMARY KEY, `name` VARCHAR(50) ,
                      `year` INT,
                      `artist` VARCHAR(50),
                      `shortname` VARCHAR(20),
                      `bpm` INT ,
                      `duration` INT,
                      `genre` VARCHAR(20) ,
                      `spotifyid` VARCHAR(20),
                      `album` VARCHAR(50)
);