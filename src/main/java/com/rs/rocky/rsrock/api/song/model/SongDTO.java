package com.rs.rocky.rsrock.api.song.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SongDTO {

  @JsonProperty("Id")
  private long id;

  @JsonProperty("Name")
  private String name;

  @JsonProperty("Year")
  private Integer year;

  @JsonProperty("Artist")
  private String artist;

  @JsonProperty("Shortname")
  private String shortName;

  @JsonProperty("Bpm")
  private Short bpm;

  @JsonProperty("Duration")
  private Long duration;

  @JsonProperty("Genre")
  private String genre;

  @JsonProperty("SpotifyId")
  private String spotifyId;

  @JsonProperty("Album")
  private String album;
}