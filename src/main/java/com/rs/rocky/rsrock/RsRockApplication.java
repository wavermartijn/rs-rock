package com.rs.rocky.rsrock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsRockApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsRockApplication.class, args);
	}

}
