package com.rs.rocky.rsrock.api.song;

import com.rs.rocky.rsrock.repositories.ArtistRepository;
import com.rs.rocky.rsrock.api.bootstrap.model.SongEntity;
import com.rs.rocky.rsrock.repositories.SongRepository;
import com.rs.rocky.rsrock.api.song.model.SongDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SongService {

  @Autowired
  ArtistRepository artistRepository;

  @Autowired
  SongRepository songRepository;

  @Autowired
  ModelMapper modelMapper;

  public List<SongDTO> getSongsWithGenre(String genre) {
    //TODO switch to enum for genre
    return songRepository.findByGenre(genre).stream().map(songEntity ->
            modelMapper.map(songEntity, SongDTO.class))
            .collect(Collectors.toList());
  }

  public SongDTO deleteSongWithId(long id) {
    Optional<SongEntity> songEntityOptional = songRepository.findById(id);
    if (songEntityOptional.isPresent()) {
      songEntityOptional.ifPresent(song -> songRepository.deleteById(id));
      return modelMapper.map(songEntityOptional.get(),SongDTO.class);
    } else {
      throw new RestClientException("Song with id "+id+" not found");
    }
  }

  public SongDTO getSongWithId(long id) {
    Optional<SongEntity> songEntityOptional = songRepository.findById(id);
    if (songEntityOptional.isPresent()) {
      return modelMapper.map(songEntityOptional.get(),SongDTO.class);
    } else {
      throw new RestClientException("Song with id "+id+" not found");
    }
  }

  public SongDTO addSong(SongDTO newSongDTO) {
    Optional<SongEntity> songEntityOptional = Optional.of(songRepository.save(modelMapper.map(newSongDTO,SongEntity.class)));
    return modelMapper.map(songEntityOptional.get(),SongDTO.class);
  }
}
