package com.rs.rocky.rsrock.api.bootstrap;

import com.rs.rocky.rsrock.api.bootstrap.model.Song;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BootstrapServiceTest {

  @Test
  void isMetalAndBefore2016() {
    BootstrapService bootstrapService = new BootstrapService();
    assertTrue(bootstrapService.isMetalAndBefore2016(generateSongWith("Metal",2010)));
    assertTrue(bootstrapService.isMetalAndBefore2016(generateSongWith("Heavy metal",2015)));
    assertTrue(bootstrapService.isMetalAndBefore2016(generateSongWith("metAl",20)));
    assertFalse(bootstrapService.isMetalAndBefore2016(generateSongWith("Metal",2017)));
    assertFalse(bootstrapService.isMetalAndBefore2016(generateSongWith("Rock",2010)));

  }


  private Song generateSongWith(String genre, int year){
    return Song.builder().artist("Bla")
            .genre(genre)
            .year(year).build();
  }
}